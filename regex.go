package main

import (
	"regexp"
	"fmt"
)

func main() {
{
	const line = "this is a line of input\n"
	var regex = regexp.MustCompile(`a line of`)
	fmt.Println(regex.MatchString(line))
}}
