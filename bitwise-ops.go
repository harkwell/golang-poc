package main

import (
	"strconv"
	"math"
	"fmt"
	"os"
)

func main() {
{
	var netmask int64
	octets := []byte { 0,0,0,0 }
	numbits,_ := strconv.ParseUint(os.Args[1], 10, 8)
	netmask = int64(math.Pow(2,32) - math.Pow(2,float64(32-numbits)))
	fmt.Printf("numbits=%d\n", numbits)
	fmt.Printf("netmask=%d %s\n", netmask, strconv.FormatInt(netmask, 2))

	for i := 4; i>0; i-- {
		octets[4-i] = byte(netmask >> ((uint16(i)-1)*8))
	}
	fmt.Printf("netmask=%d.%d.%d.%d\n", octets[0], octets[1], octets[2],
		octets[3])
}}
