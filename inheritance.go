package main

import (
	"reflect"
	"fmt"
)

type A interface
{
	AMethod1() string
}

type B interface
{
	BMethod1(token string) bool
}

type AB interface
{
	A
	B
}

type AS struct
{
	name string
}

type BS struct
{
	val int
}

type ABS struct
{
	AS
	BS
}

func (this AS) AMethod1() string {
{
	return this.name
}}

// func (this *ABS) BMethod1(token string) bool {  <---  inheritsFromAB() false!
func (this ABS) BMethod1(token string) bool {
{
	fmt.Printf("abs (this) is: %s\n", this)
	return (token == this.AMethod1())
}}

func inheritsFromAB(a A) bool {
{
	type1 := reflect.TypeOf((*AB)(nil)).Elem()
	type2 := reflect.TypeOf(a)
	return type2.AssignableTo(type1)

}}

func inheritsFromABandIsNamed(a A, name string) bool {
{
	retval := false

	if retval = inheritsFromAB(a); retval {
		abs,_ := a.(ABS)
		fmt.Printf("abs is: %s\n", abs)
		retval = abs.BMethod1(name)

	} else {
		fmt.Printf("a does not inherit from AB\n")
	}
	return retval
}}

func main() {
{
	ab := ABS{AS{"myname"},BS{7}}
	fmt.Printf("ab is: %s\n", ab)
	fmt.Printf("ab name: \"%s\"\n", ab.AMethod1())
	fmt.Printf("%d\n", inheritsFromABandIsNamed(ab, "myname"))
}}
