package main

import (
	"strings"
	"bufio"
	"fmt"
	"os"
)

func main() {
{
	const input = "yuck, yuck, yuck\nhow now brown cow.\n"
	scanner := bufio.NewScanner(strings.NewReader(input))

	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}}
