package main

import (
	"runtime"
	"fmt"
)

func main() {
{
	fmt.Printf("GOOS: %s\n", runtime.GOOS)
	fmt.Printf("GOARCH: %s\n", runtime.GOARCH)
}}
