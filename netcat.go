package main

// checks for socket "netcat -z host port && echo yep"
// go build netcat.go
// ./netcat 127.0.0.1 80 && echo yep

import (
	"net"
	"fmt"
	"os"
)

func main() {
{
	host := os.Args[1]
	port := os.Args[2]
	// fmt.Printf("connecting to: %s:%s\n", host, port)
	conn, err := net.Dial("tcp", host+":"+port)

	if conn != nil {
		defer conn.Close()
	}
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	os.Exit(0)
}}
