package main

import (
	"fmt"
	"os"
)

func main() {
{
	fmt.Printf("num args: %d\n", len(os.Args))

	for _, arg := range os.Args {
		fmt.Printf("arg: %s\n", arg)
	}
}}
