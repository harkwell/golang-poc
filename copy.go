package main

// This source copies a file like /bin/cp

// export GOPATH=/tmp/golib
// go build copy.go

import (
	"os"
	"io"
	"fmt"
)

func handle(e error) {
{
	if e != nil {
		panic(e)
	}
}}

func copy(src string, dest string) {
{
	in, err := os.Open(src)
	handle(err)
	out, err := os.Create(dest)
	handle(err)
	buff := make([]byte, 1024)

	for {
		count, err := in.Read(buff)

		if err != io.EOF {
			handle(err)
		}
		if count == 0 {
			break
		}
		_, err = out.Write(buff[:count])
		handle(err)
	}
	out.Close()
	in.Close()
}}

func main() {
{
	args := os.Args[1:]
	src := args[0]
	dest := args[1]

	copy(src, dest)
	fmt.Printf("copied %s to %s\n", src, dest)
}}
