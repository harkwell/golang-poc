package main

import "fmt"
 
type I interface
{
	m1()
}
 
type Base struct
{
}
 
func (this Base) m1() {
{
	fmt.Printf("hello %s world\n", this.m2())
}}
 
func (this Base) m2() string {
{
	return ""
}}
 
type Subclass struct
{
	Base
}
 
func (e Subclass) m2() string {
{
	return "missing"
}}
 
func main() {
{
	var i I
	i = Subclass { }
	i.m1()
}}
