package main

// go build json.go
// echo '{"foo":"bar","doh":"baz", "i":[{"ifoo":"ibar"}]}' |./json

import (
	"encoding/json"
	"fmt"
	"os"
)

type Mytype1 struct
{
	Foo string
	Doh string
	I []Mytype2
}

type Mytype2 struct
{
	Ifoo string
}

func main() {
{
	decoder := json.NewDecoder(os.Stdin)
	var obj Mytype1
	err := decoder.Decode(&obj)

	if err != nil {
		panic(err)
	}
	fmt.Printf("key=\"foo\" val=\"%s\"\n", obj.Foo)
	fmt.Printf("key=\"doh\" val=\"%s\"\n", obj.Doh)

	for _, val := range obj.I {
		fmt.Printf("key=\"i.ifoo\" val=\"%s\"\n", val.Ifoo)
	}
}}
