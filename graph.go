package main

// go get github.com/thcyron/graphs
// go build graph.go

import (
	graph "github.com/thcyron/graphs"
	"fmt"
)

func main() {
{
	g := graph.NewGraph()
	g.AddEdge(1, 2, 1)
	g.AddEdge(2, 3, 2)
	g.AddEdge(3, 4, 3)
	g.AddEdge(2, 5, 4)
	g.AddEdge(5, 6, 5)
	g.AddEdge(2, 7, 6)
	g.AddEdge(2, 8, 7)
	g.AddEdge(8, 9, 8)
	g.AddEdge(9, 10, 9)
	graph.DFS(g, 1, func(v graph.Vertex, stop *bool) {
		{
			fmt.Println(v)
		}})
	fmt.Println(g)
}}
