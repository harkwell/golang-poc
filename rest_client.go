package main

import (
	"io/ioutil"
	"net/http"
	"bytes"
	"fmt"
)

func main() {
{
	var json = []byte(`{"title":"another one bytes th3 du$7..."}`)
	url := "http://localhost:8080/foo"
	fmt.Println("URL:>", url)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(json))
	req.Header.Set("Content-Type", "application/json")
	client := &http.Client{}
	rslt, err := client.Do(req)

	if err != nil {
		panic(err)
	}
	defer rslt.Body.Close()
	fmt.Println("response Status:", rslt.Status)
	fmt.Println("response Headers:", rslt.Header)
	content, _ := ioutil.ReadAll(rslt.Body)
	fmt.Println("response Body:", string(content))
}}
