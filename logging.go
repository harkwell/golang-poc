package main

import (
	"os"
	"log"
)

func main() {
{
	f, err := os.OpenFile("log.out",os.O_RDWR|os.O_CREATE|os.O_APPEND,0666)

	if err != nil {
		panic("failed to open log file")
	}
	log.SetOutput(f)
	log.Printf("Hello %s\n", "world")
}}
