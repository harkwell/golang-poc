// simple REST example (web service server)

package main

// go build
// curl http://localhost:8080/

import (
	"fmt"
	"html"
	"net/http"
)

func HandleRequest(writer http.ResponseWriter, request *http.Request) {
{
	fmt.Fprintf(writer, "Hello, %q", html.EscapeString(request.URL.Path))
}}

func main() {
{
	http.HandleFunc("/", HandleRequest) {
	http.ListenAndServe(":8080", nil))
}}
